﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FitupBackend.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FitupBackend.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class RecordsController : ControllerBase
    {
        private readonly FitupContext _db;
        private readonly AuthService _auth;

        public RecordsController(FitupContext db, AuthService auth)
        {
            _db = db;
            _auth = auth;
        }
        
        [HttpPost("add")]
        public async Task<IActionResult> AddRecord(long dateTimestamp, int value)
        {
            if (HttpContext.User == null)
                return Unauthorized();
            
            User user = await _auth.ValidateAsync(HttpContext.User);
            if (user == null)
                return Unauthorized();

            DateTime date = DateTimeOffset.FromUnixTimeSeconds(dateTimestamp).DateTime;
            
            await _db.Records.AddAsync(new Record
            {
                CreationDate = date,
                Value = value,
                UserId = user.Id
            });
            
            await _db.SaveChangesAsync();
            
            return Ok(new WeightRecord(value, dateTimestamp));
        }

        [HttpGet("get_all_records")]
        public async Task<IActionResult> GetAllRecords()
        {
            if (HttpContext.User == null)
                return Unauthorized();
            
            User user = await _auth.ValidateAsync(HttpContext.User);
            
            List<Record> records = await _db.Records.Where(x => x.UserId == user.Id).ToListAsync();
            List<WeightRecord> weightRecords = new ();
            
            foreach (var record in records)
            {
                weightRecords.Add(new WeightRecord(record.Value, (long)(record.CreationDate - new DateTime(1970, 1, 1)).TotalSeconds));
            }
            
            return Ok(weightRecords);
        }
    }
}