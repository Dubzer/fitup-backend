﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using FitupBackend.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Serilog;

namespace FitupBackend.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class LoginController : ControllerBase
    {
        private readonly FitupContext _db;
        private readonly AuthService _auth;

        public LoginController(FitupContext db,AuthService auth)
        {
            _db = db;
            _auth = auth;
        }

        [HttpPost("register")]
        public async Task<IActionResult> Register([Required] [EmailAddress] string username, [Required] string password)
        {
            if (await _db.Users.AnyAsync(x => x.Username == username))
                return Forbid();
            
            var salt = Guid.NewGuid().ToString().Replace("-", "");
            var encodedbyte = new SHA1Managed().ComputeHash(Encoding.UTF8.GetBytes(password + salt));
            var hash = string.Concat(encodedbyte.Select(b => b.ToString("x2")));

            var user = await _db.Users.AddAsync(new ()
            {
                Username = username,
                Password = hash,
                Salt = salt
            });
            
            _db.SaveChangesAsync();

            var token = _auth.Generate(user.Entity.Id, hash);
            return Ok(token);
        }
        
        [HttpGet]
        public async Task<IActionResult> Authorize([Required] string username, [Required] string password)
        {
            var user = _db.Users.FirstOrDefault(x => x.Username == username);
            if (user == null)
            {
                Log.Error("User was null");
                return Unauthorized();
            }
            
            var encodedbyte = new SHA1Managed().ComputeHash(Encoding.UTF8.GetBytes(password + user.Salt));
            var encodedstring = string.Concat(encodedbyte.Select(b => b.ToString("x2")));
            if (encodedstring != user.Password)
            {
                Log.Information($"Failed login attempt to {username}. Wrong password.");
                return Unauthorized();
            }

            var token = _auth.Generate(user.Id, user.Password);
            
            return Ok(token);
        }

        [HttpGet("check")]
        public async Task<IActionResult> Check()
        {
            if (HttpContext.User == null)
            {
                Log.Error("User was null");
                return Unauthorized();
            }
            
            User user = await _auth.ValidateAsync(HttpContext.User);
            if (user == null)
            {
                Log.Error("User was null");
                return Unauthorized();
            }

            return Ok();
        }
    }
}