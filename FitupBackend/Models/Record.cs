﻿using System;

#nullable disable

namespace FitupBackend
{
    public partial class Record
    {
        public int Id { get; set; }
        public DateTime CreationDate { get; set; }
        public int UserId { get; set; }
        public int Value { get; set; }
    }
}
