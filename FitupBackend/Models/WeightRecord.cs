﻿namespace FitupBackend.Models
{
    public record WeightRecord(int Value, long Date);
}