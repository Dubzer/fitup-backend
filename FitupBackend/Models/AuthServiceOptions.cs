﻿namespace FitupBackend.Models
{
    public class AuthServiceOptions
    {
        public string Key { get; set; }
        public string Issuer { get; set; }
        public string Audience { get; set; }
    }
}