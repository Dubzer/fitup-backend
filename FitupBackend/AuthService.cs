﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using FitupBackend.Models;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace FitupBackend
{
    public class AuthService
    {
        private readonly FitupContext _context;
        private readonly string _issuer;
        private readonly string _audience;
        private readonly SigningCredentials _credentials;
        
        public AuthService(FitupContext context, IOptions<AuthServiceOptions> options)
        {
            _context = context;
            _issuer = options.Value.Issuer;
            _audience = options.Value.Audience;
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(options.Value.Key));
            _credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
        }
        
        public string Generate(int id, string hash, int expiresDays = 31)
        {
            if(expiresDays < 0) throw new ArgumentException("Invalid expiredDays.", nameof(expiresDays));
            return new JwtSecurityTokenHandler().WriteToken(new JwtSecurityToken(_issuer, _audience, new List<Claim>
                {
                    new("id", id.ToString()),
                    new("hash", hash),
                }, expires: expiresDays != 0 ? DateTime.Now.AddDays(expiresDays) : (DateTime?)null,
                signingCredentials: _credentials));
        }
        public async Task<User> ValidateAsync(ClaimsPrincipal user)
        {
            if (!user.HasClaim(c => c.Type == "id")) return null;
            if (!user.HasClaim(c => c.Type == "hash")) return null;
            
            var entity = await _context.Users.FindAsync(int.Parse(user.Claims.First(x => x.Type == "id").Value));
            var hash = user.Claims.First(x => x.Type == "hash").Value;
            
            if (entity == null) return null;
            if (hash != entity.Password) return null;
            return entity;
        }
    }
}