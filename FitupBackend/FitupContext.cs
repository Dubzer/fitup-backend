﻿using FitupBackend.Models;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace FitupBackend
{
    public partial class FitupContext : DbContext
    {
        public FitupContext()
        {
        }

        public FitupContext(DbContextOptions<FitupContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Record> Records { get; set; }
        public virtual DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "en_US.utf8");

            modelBuilder.Entity<Record>(entity =>
            {
                entity.ToTable("records");

                entity.HasIndex(e => e.Id, "records_id_uindex")
                    .IsUnique();

                entity.HasIndex(e => e.UserId, "records_user_id_uindex")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CreationDate)
                    .HasColumnType("timestamp with time zone")
                    .HasColumnName("creation_date");

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.Property(e => e.Value).HasColumnName("value");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("users");

                entity.HasIndex(e => e.Username, "users_username_uindex")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasColumnName("password");

                entity.Property(e => e.Salt)
                    .IsRequired()
                    .HasColumnName("salt");

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasColumnName("username");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
